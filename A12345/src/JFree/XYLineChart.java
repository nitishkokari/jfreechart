package JFree;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleEdge;

public class XYLineChart extends JFrame {
	private static final long serialVersionUID = 1L;

	public void Time() {
	    setSize(800, 600);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);

	    ClockLabel clock = new ClockLabel();
	    JPanel customPanel1 = new JPanel();
	    clock.setFont(new Font("Monotype Corsiva",1,28));
	    clock.setPreferredSize(new Dimension(400,20));
	    customPanel1.add(clock);
	    this.add(customPanel1, BorderLayout.SOUTH);
	  }

	private void display(){
		JPanel customPanel = new JPanel();
        String s = "Welcome To StockBucks";
        JLabel f = new JLabel(s);
        
        MarqueePanel mp = new MarqueePanel(s, 30);
        customPanel.add(mp);
        f.setPreferredSize(new Dimension(400,30));
        this.add(customPanel,BorderLayout.NORTH);
        //f.setVisible(true);
        mp.start();
	}
	
	public XYLineChart() throws SQLException {

		super("XY Line Chart Example with JFreechart");
		
		
		JPanel chartPanel = createChartPanel();
		add(chartPanel, BorderLayout.CENTER);
			
		setSize(640, 480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
	}


	class ClockLabel extends JLabel implements ActionListener {

		  /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ClockLabel() {
		    super("" + new Date());
		    Timer t = new Timer(1000, this);
		    t.start();
		  }

		  public void actionPerformed(ActionEvent ae) {
		    setText((new Date()).toString());
		  }
		}
	
	class MarqueePanel extends JPanel implements ActionListener {

		   private static final long serialVersionUID = 1L;
			private static final int RATE = 12;
		    private final Timer timer = new Timer(2000 / RATE, this);
		    private final JLabel label = new JLabel();
		    private final String s;
		    private final int n;
		    private int index;

		    public MarqueePanel(String s, int n) {
		        if (s == null || n < 1) {
		            throw new IllegalArgumentException("Null string or n < 1");
		        }
		        StringBuilder sb = new StringBuilder(n);
		        for (int i = 0; i < n; i++) {
		            sb.append(' ');
		        }
		        this.s = sb + s + sb;
		        this.n = n;
		        label.setFont(new Font("Monotype Corsiva", Font.ITALIC, 24));
		        label.setText(sb.toString());
		        this.add(label);
		    }

		    public void start() {
		        timer.start();
		    }

		    public void stop() {
		        timer.stop();
		    }

		    public void actionPerformed(ActionEvent e) {
		        index++;
		        if (index > s.length() - n) {
		            index = 0;
		        }
		        label.setText(s.substring(index, index + n));
		    }
	}
	
	private JPanel createChartPanel() throws SQLException {
		String chartTitle = "STOCK EXCHANGE";
		String xAxisLabel = "Time";
		String yAxisLabel = "Rate";
		
		XYDataset dataset = createDataset();
		
		JFreeChart chart = ChartFactory.createXYLineChart(chartTitle, 
				xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL, true, true,true);
		
		customizeChart(chart);
		
		// saves the chart as an image files
		File imageFile = new File("XYLineChart.png");
		int width = 640;
		int height = 480;
		
		try {
			ChartUtilities.saveChartAsPNG(imageFile, chart, width, height);
		} catch (IOException ex) {
			System.err.println(ex);
		}
		
		return new ChartPanel(chart);
	}

	private XYDataset createDataset() throws SQLException {
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series1 = new XYSeries("NIFTY");
		XYSeries series2 = new XYSeries("NIFUT");
		XYSeries series3 = new XYSeries("BanEX");
		XYSeries series4 = new XYSeries("SBI");
		XYSeries series5 = new XYSeries("ICICI");
		XYSeries series6 = new XYSeries("Axis");
		XYSeries series7 = new XYSeries("YES");
		XYSeries series8 = new XYSeries("INFY");
		XYSeries series9 = new XYSeries("TCS");
		XYSeries series10 = new XYSeries("L&T");
		XYSeries series11 = new XYSeries("STOCKBUCKS");
		
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila","root","Nk8867666820");
		System.out.println("Connection Established");
		
		String a[]={"T930","T10","T1030","T11","T1130","T12","T1230"};
		double z[]={9.30,10.00,10.30,11.00,11.30,12.00,12.30}; 
		String query1 = "SELECT * FROM CompanyDetails WHERE id=1";
		Statement stmt1 = conn.createStatement();
		ResultSet rs1 = stmt1.executeQuery(query1);
		int i=0;
		rs1.next();
		float str[]={0,0,0,0,0,0,0};
		while(i<7){
		str[i] = Float.parseFloat(rs1.getString(a[i]));
		series1.add(z[i], str[i++]);
		}
		
			String query2 = "SELECT * FROM CompanyDetails WHERE id=2";
			Statement stmt2 = conn.createStatement();
			ResultSet rs2 = stmt2.executeQuery(query2);
			i=0;
			rs2.next();
			float str1[]={0,0,0,0,0,0,0};
			while(i<7)
			{
			str1[i] = Float.parseFloat(rs2.getString(a[i]));
			series2.add(z[i], str1[i++]);
			}
			
				String query3 = "SELECT * FROM CompanyDetails WHERE id=3";
				Statement stmt3 = conn.createStatement();
				ResultSet rs3 = stmt3.executeQuery(query3);
				i=0;
				rs3.next();
				float str2[]={0,0,0,0,0,0,0};
				while(i<7)
				{
				str2[i] = Float.parseFloat(rs3.getString(a[i]));
				series3.add(z[i], str2[i++]);
				}
				
					String query4 = "SELECT * FROM CompanyDetails WHERE id=4";
					Statement stmt4 = conn.createStatement();
					ResultSet rs4 = stmt4.executeQuery(query4);
					i=0;
					rs4.next();
					float str3[]={0,0,0,0,0,0,0};
					while(i<7)
					{
					str3[i] = Float.parseFloat(rs4.getString(a[i]));
					series4.add(z[i], str3[i++]);
					}
					
						String query5 = "SELECT * FROM CompanyDetails WHERE id=5";
						Statement stmt5 = conn.createStatement();
						ResultSet rs5 = stmt5.executeQuery(query5);
						i=0;
						rs5.next();
						float str4[]={0,0,0,0,0,0,0};
						while(i<7)
						{
						str4[i] = Float.parseFloat(rs5.getString(a[i]));
						series5.add(z[i], str4[i++]);
						}
						
							String query6 = "SELECT * FROM CompanyDetails WHERE id=6";
							Statement stmt6 = conn.createStatement();
							ResultSet rs6 = stmt6.executeQuery(query6);
							i=0;
							rs6.next();
							float str5[]={0,0,0,0,0,0,0};
							while(i<7)
							{
							str5[i] = Float.parseFloat(rs6.getString(a[i]));
							series6.add(z[i], str5[i++]);
							}
							
								String query7 = "SELECT * FROM CompanyDetails WHERE id=7";
								Statement stmt7 = conn.createStatement();
								ResultSet rs7 = stmt7.executeQuery(query7);
								i=0;
								rs7.next();
								float str6[]={0,0,0,0,0,0,0};
								while(i<7)
								{
								str6[i] = Float.parseFloat(rs7.getString(a[i]));
								series7.add(z[i], str6[i++]);
								}
								
									String query8 = "SELECT * FROM CompanyDetails WHERE id=8";
									Statement stmt8 = conn.createStatement();
									ResultSet rs8 = stmt8.executeQuery(query8);
									i=0;
									rs8.next();
									float str7[]={0,0,0,0,0,0,0};
									while(i<7)
									{
									str7[i] = Float.parseFloat(rs8.getString(a[i]));
									series8.add(z[i], str7[i++]);
									}
									
										String query9 = "SELECT * FROM CompanyDetails WHERE id=9";
										Statement stmt9 = conn.createStatement();
										ResultSet rs9 = stmt9.executeQuery(query9);
										i=0;
										rs9.next();
										float str8[]={0,0,0,0,0,0,0};
										while(i<7)
										{
										str8[i] = Float.parseFloat(rs9.getString(a[i]));
										series9.add(z[i], str8[i++]);
										}
										
											String query10 = "SELECT * FROM CompanyDetails WHERE id=10";
											Statement stmt10 = conn.createStatement();
											ResultSet rs10 = stmt10.executeQuery(query10);
											i=0;
											rs10.next();
											float str9[]={0,0,0,0,0,0,0};
											while(i<7)
											{
											str9[i] = Float.parseFloat(rs10.getString(a[i])); 
											series10.add(z[i], str9[i++]);
											}
											String query11 = "SELECT * FROM CompanyDetails WHERE id=0";
											Statement stmt11 = conn.createStatement();
											ResultSet rs11 = stmt11.executeQuery(query11);
											i=0;
											rs11.next();
											float str10[]={0,0,0,0,0,0,0};
											while(i<7)
											{
											str10[i] = Float.parseFloat(rs11.getString(a[i])); 
											series11.add(z[i], str10[i++]);
											}
											
		
		dataset.addSeries(series1);
		dataset.addSeries(series2);
		dataset.addSeries(series3);
		dataset.addSeries(series4);
		dataset.addSeries(series5);
		dataset.addSeries(series6);
		dataset.addSeries(series7);
		dataset.addSeries(series8);
		dataset.addSeries(series9);
		dataset.addSeries(series10);
		dataset.addSeries(series11);
		
		return dataset;
	}
	
	private void customizeChart(JFreeChart chart) {
		
		final ChartPanel chartPanel = new ChartPanel(chart);
        this.add(chartPanel, BorderLayout.CENTER);
        
        chartPanel.repaint();
        
        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setVerticalAxisTrace(false);       
        
		XYPlot plot = chart.getXYPlot();
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
       
        LegendTitle legend =  chart.getLegend();
        legend.setPosition(RectangleEdge.LEFT);
		JLabel item;
	    item = new JLabel();
	    add(item);
	 
	        
	    final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
	    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
	    rangeAxis.setAutoRangeIncludesZero(false);
	    rangeAxis.setVerticalTickLabels(false);
	    rangeAxis.setAutoTickUnitSelection(rootPaneCheckingEnabled);   
	       
		// sets paint color for each series
		renderer.setSeriesPaint(0, Color.RED);
		renderer.setSeriesPaint(1, Color.GREEN);
		renderer.setSeriesPaint(2, Color.YELLOW);
		renderer.setSeriesPaint(3, Color.BLUE);
		renderer.setSeriesPaint(4, Color.GREEN);
		renderer.setSeriesPaint(5, Color.GRAY);
		renderer.setSeriesPaint(6, Color.RED);
		renderer.setSeriesPaint(7, Color.CYAN);
		renderer.setSeriesPaint(8, Color.YELLOW);
		renderer.setSeriesPaint(9, Color.BLUE);
		renderer.setSeriesPaint(10, Color.BLACK);
		
		// sets thickness for series (using strokes)
		renderer.setSeriesStroke(0, new BasicStroke(2.0f));
		renderer.setSeriesStroke(1, new BasicStroke(3.0f));
		renderer.setSeriesStroke(2, new BasicStroke(2.0f));
		renderer.setSeriesStroke(3, new BasicStroke(2.0f));
		renderer.setSeriesStroke(4, new BasicStroke(3.0f));
		renderer.setSeriesStroke(5, new BasicStroke(2.0f));
		renderer.setSeriesStroke(6, new BasicStroke(2.0f));
		renderer.setSeriesStroke(7, new BasicStroke(3.0f));
		renderer.setSeriesStroke(8, new BasicStroke(2.0f));
		renderer.setSeriesStroke(9, new BasicStroke(2.0f));
		renderer.setSeriesStroke(10, new BasicStroke(4.0f));
		
		
		renderer.setBaseItemLabelsVisible(false);
        renderer.setBaseToolTipGenerator(new StandardXYToolTipGenerator());
		
		IntervalMarker marker = new IntervalMarker(0, 1.5, new Color(0, 0, 0, 255),
        		new BasicStroke(), new Color(0, 0, 130, 255), new BasicStroke(), 0.4f);
        plot.addRangeMarker(marker);
        IntervalMarker marker1 = new IntervalMarker(4.5, 6, new Color(0, 0, 0, 255),
        		new BasicStroke(), new Color(0, 0, 130, 255), new BasicStroke(), 0.4f);
        plot.addRangeMarker(marker1);
        
        renderer.setBaseShapesFilled(false);
        renderer.setBaseShapesVisible(false);
        renderer.setDefaultEntityRadius(20);
        
       	// sets paint color for plot outlines
		plot.setOutlinePaint(Color.RED);
		plot.setOutlineStroke(new BasicStroke(2.0f));
		
		// sets renderer for lines
		plot.setRenderer(renderer);
		
		// sets plot background
		plot.setBackgroundPaint(Color.WHITE);
		
		// sets paint color for the grid lines
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.BLACK);
		
		plot.setDomainGridlinesVisible(false);
		plot.setDomainGridlinePaint(Color.BLACK);
		
		 //You can also customize your lines like dotted one, dash-ed one etc.
        renderer.setSeriesStroke(
            0, new BasicStroke(
                2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                1.0f, new float[] {10.0f, 6.0f}, 0.0f
            )
        );
        renderer.setSeriesStroke(
            1, new BasicStroke(
                2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                1.0f, new float[] {6.0f, 6.0f}, 0.0f
            )
        );
        renderer.setSeriesStroke(
            2, new BasicStroke(
                2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                1.0f, new float[] {2.0f, 6.0f}, 0.0f
            )
        );
        renderer.setSeriesStroke(
                3, new BasicStroke(
                    2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                    1.0f, new float[] {2.0f, 6.0f}, 0.0f
                )
            );
        renderer.setSeriesStroke(
                4, new BasicStroke(
                    2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                    1.0f, new float[] {2.0f, 6.0f}, 0.0f
                )
            );
        renderer.setSeriesStroke(
                10, new BasicStroke(
                    2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
                    1.0f, new float[] {10.0f, 6.0f}, 0.0f
                )
            );
   }
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run(){
				try {
					
					XYLineChart LC = new XYLineChart();
					LC.setVisible(true);
					LC.Time();
					LC.display();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
		});
		
	}
}